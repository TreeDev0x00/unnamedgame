package com.tree.game;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import com.tree.game.camera.GameCamera;
import com.tree.game.display.Display;
import com.tree.game.entities.player.EntityPlayer;
import com.tree.game.graphics.Assets;
import com.tree.game.input.InputManager;
import com.tree.game.menu.MenuManager;
import com.tree.game.world.WorldManager;

public class Game implements Runnable {

	private Thread thread;
	private Display display;
	private BufferStrategy bs;
	private Graphics g;
	private Assets assets;
	private MenuManager menuManager;
	private EntityPlayer thePlayer;
	private InputManager inputManager;
	private WorldManager worldManager;
	private GameCamera gameCamera;
	
	private boolean isRunning = false;
	private int currentFps;
	
	//Change this to player setting in the future
	private int gameFps = 60;

	public Game() {}
	
	private void init() {
		assets = new Assets();
		inputManager = new InputManager(this);
		display = new Display(this, 640, 640);
		gameCamera = new GameCamera();
		thePlayer = new EntityPlayer(this, 250, -350, -350);
		menuManager = new MenuManager(this);
		menuManager.setCurrentMenu(menuManager.getGameMenu());
		worldManager = new WorldManager(this);
	}
	
	private void update() {
		menuManager.getCurrentMenu().update();
	}
	
	private void render() {
		int canvasWidth = display.getCanvas().getWidth();
		int canvasHeight = display.getCanvas().getHeight();
		
		bs = display.getCanvas().getBufferStrategy();
		
		if (bs == null) {
			display.getCanvas().createBufferStrategy(3);
			return;
		}

		g = bs.getDrawGraphics();
		g.clearRect(0, 0, canvasWidth, canvasHeight);
		menuManager.getCurrentMenu().render(g);
		
		bs.show();
		g.dispose();
	}

	@Override
	public void run() {
		init();
		
		double timePerTick = 1000000000 / gameFps;
		double delta = 0;
		long now;
		long lastTime = System.nanoTime();
		long timer = 0;
		int ticks = 0;
		
		while (isRunning) {
			now = System.nanoTime();
			delta += (now - lastTime) / timePerTick;
			timer += now - lastTime;
			lastTime = now;
			
			if (delta >= 1) {
				update();
				render();
				ticks++;
				delta--;
			}
			
			if (timer >= 1000000000) {
				currentFps = ticks;
				System.out.println(currentFps + " FPS");
				ticks = 0;
				timer = 0;
			}
		}
		
		stop();
	}
	
	public synchronized void start() {
		if (!isRunning) {
			isRunning = true;
			thread = new Thread(this);
			thread.start();
		}
	}

	public synchronized void stop() {
		if (isRunning) {
			isRunning = false;
			try {
				thread.join();
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public GameCamera getGameCamera() {
		return gameCamera;
	}
	
	public WorldManager getWorldManager() {
		return worldManager;
	}	
	
	public EntityPlayer getThePlayer() {
		return thePlayer;
	}
	
	public Display getDisplay() {
		return display;
	}
	
	public InputManager getInputManager() {
		return inputManager;
	}
	
	public MenuManager getMenuManager() {
		return menuManager;
	}
	
	public Assets getAssets() {
		return assets;
	}
	
	public int getGameFps() {
		return gameFps;
	}
	
	public int getCurrentFps() {
		return currentFps;
	}

}
