package com.tree.game.display;

import java.awt.Canvas;

import javax.swing.JFrame;

import com.tree.game.Game;

public class Display {

	private JFrame frame;
	private Canvas canvas;
	
	private static final String TITLE = "Game v1.0";
	
	public Display(Game game, int width, int height) {
		createDisplay(game, width, height);
	}
	
	private void createDisplay(Game game, int width, int height) {
		frame = new JFrame(TITLE);
		frame.setSize(width, height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		canvas = new Canvas();
		canvas.addKeyListener(game.getInputManager().getKeyboardListener());
		canvas.setSize(width, height);
		frame.add(canvas);
		frame.pack();
	}
	
	public Canvas getCanvas() {
		return canvas;
	}
	
}
