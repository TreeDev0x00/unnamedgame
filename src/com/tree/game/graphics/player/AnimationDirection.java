package com.tree.game.graphics.player;

public enum AnimationDirection {
	FORWARD, BACKWARD, LEFT, RIGHT;
}
