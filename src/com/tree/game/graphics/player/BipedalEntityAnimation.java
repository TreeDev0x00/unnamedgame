package com.tree.game.graphics.player;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tree.game.graphics.SpriteSheet;

public class BipedalEntityAnimation {

	private SpriteSheet spriteSheet;
	private AnimationDirection direction;
	private boolean frozen;
	private int speed;
	
	private List<BufferedImage> forwardSprites;
	private List<BufferedImage> backwardSprites;
	private List<BufferedImage> leftSprites;
	private List<BufferedImage> rightSprites;
	
	private int currentIndex = 0;
	private long lastAnimationTime = 0;
	
	public BipedalEntityAnimation(SpriteSheet spriteSheet, AnimationDirection direction, int f) {
		this.spriteSheet = spriteSheet;
		this.direction = direction;
		this.speed = f;
		setupSprites();
	}
	
	public void update() {
		long now = System.currentTimeMillis();
		
		if ((lastAnimationTime + speed) <= now && !frozen) {
			lastAnimationTime = now;
			currentIndex++;
			switch(direction) {
			case FORWARD:
				if (currentIndex >= forwardSprites.size()) {
					currentIndex = 0;
				}
				break;
			case BACKWARD:
				if (currentIndex >= backwardSprites.size()) {
					currentIndex = 0;
				}
				break;
			case LEFT:
				if (currentIndex >= leftSprites.size()) {
					currentIndex = 0;
				}
				break;
			case RIGHT:
				if (currentIndex >= rightSprites.size()) {
					currentIndex = 0;
				}
				break;
			}
		}
	}
	
	private void setupSprites() {
		forwardSprites = new ArrayList<>();
		backwardSprites = new ArrayList<>();
		leftSprites = new ArrayList<>();
		rightSprites = new ArrayList<>();
		
		forwardSprites = Arrays.asList(spriteSheet.getSubImage(12), spriteSheet.getSubImage(13), 
				spriteSheet.getSubImage(14), spriteSheet.getSubImage(15));
		backwardSprites = Arrays.asList(spriteSheet.getSubImage(0), spriteSheet.getSubImage(1), 
				spriteSheet.getSubImage(2), spriteSheet.getSubImage(3));
		leftSprites = Arrays.asList(spriteSheet.getSubImage(4), spriteSheet.getSubImage(5), 
				spriteSheet.getSubImage(6), spriteSheet.getSubImage(7));
		rightSprites = Arrays.asList(spriteSheet.getSubImage(8), spriteSheet.getSubImage(9), 
				spriteSheet.getSubImage(10), spriteSheet.getSubImage(11));
	}
	
	public void freezeAnimation() {
		currentIndex = 0;
		frozen = true;
	}
	
	public void unfreezeAnimation() {
		frozen = false;
	}

	public void setAnimationDirection(AnimationDirection direction) {
		if (this.direction == direction) {
			return;
		}
		
		currentIndex = 0;
		lastAnimationTime = 0;
		this.direction = direction;
	}

	public BufferedImage getLatestFrame() {
		switch(direction) {
		case FORWARD:
			return forwardSprites.get(currentIndex);
		case BACKWARD:
			return backwardSprites.get(currentIndex);
		case LEFT:
			return leftSprites.get(currentIndex);
		case RIGHT:
			return rightSprites.get(currentIndex);
		}
		return null;
	}
	
}
