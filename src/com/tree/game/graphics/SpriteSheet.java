package com.tree.game.graphics;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class SpriteSheet {

	private BufferedImage sheet;
	private List<BufferedImage> subImages;
	private int spriteWidth;
	private int spriteHeight;
	
	public SpriteSheet(BufferedImage sheet, int spriteWidth, int spriteHeight) {
		this.spriteWidth = spriteWidth;
		this.spriteHeight = spriteHeight;
		this.sheet = sheet;
		this.subImages = new ArrayList<>();
		setupSubImages();
	}
	
	public BufferedImage getSubImage(int index) {
		return subImages.get(index);
	}
	
	public int getSpriteWidth() {
		return spriteWidth;
	}
	
	public int getSpriteHeight() {
		return spriteHeight;
	}
	
	private void setupSubImages() {
		for (int y = 0; y < sheet.getHeight() / spriteHeight; y++) {
			for (int x = 0; x < sheet.getWidth() / spriteWidth; x++) {
				int xLoc = x * spriteWidth;
				int yLoc = y * spriteHeight;
				subImages.add(sheet.getSubimage(xLoc, yLoc, spriteWidth, spriteHeight));
			}
		}
	}
	
}
