package com.tree.game.graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.tree.game.utils.WorldUtil;

public class Assets {

	private Map<String, BufferedImage> tiles;
	private Map<String, BufferedImage> playerSprites;
	private Map<String, int[][]> worlds;
	
	public Assets() {
		loadTiles();
		loadPlayerSprites();
		loadWorlds();
	}
	
	private void loadTiles() {
		tiles = new HashMap<>();
		tiles.put("grassTile", loadImage("/images/tiles/grass.png"));
		for (int index = 0; index < 16; index++) {
			tiles.put("tree0" + index, loadImage("/images/tiles/tree0" + index + ".png"));
		}
		
		for (int index = 0; index < 64; index++) {
			tiles.put("house0" + index, loadImage("/images/tiles/house0" + index + ".png"));
		}
	}
	
	private void loadPlayerSprites() {
		playerSprites = new HashMap<>();
		playerSprites.put("player00", loadImage("/images/player/player00.png"));
	}
	
	private void loadWorlds() {
		worlds = new HashMap<>();
		worlds.put("world00", WorldUtil.loadWorld(Assets.class.getResourceAsStream("/worlds/world00.dat")));
	}
	
	public Map<String, int[][]> getLoadedWorldData() {
		return worlds;
	}
	
	public BufferedImage getPlayerSprite(String key) {
		return playerSprites.get(key);
	}
	
	public BufferedImage getTile(String key) {
		return tiles.get(key);
	}
	
	private BufferedImage loadImage(String path) {
		try {
			return ImageIO.read(Assets.class.getResourceAsStream(path));
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(1);
		}
		return null;
	}
	
}
