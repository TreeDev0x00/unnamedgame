package com.tree.game.tiles.sets;

import com.tree.game.tiles.Tile;

public class Tree0TileSet extends TileSet {
	
	public Tree0TileSet(int startId) {
		super(startId);
		
		int id = getStartId();
		String prefix = "tree0";
		
		for (int index = 0; index < 8; index++) {
			addTile(new Tile(id, prefix + index, false, true, 0));
			id++;
		}
		
		for (int index = 8; index < 12; index++) {
			addTile(new Tile(id, prefix + index, true, false, -1));
			id++;
		}
		
		for (int index = 12; index < 16; index++) {
			addTile(new Tile(id, prefix + index, false, false, -1));
			id++;
		}
		
		setLastId(id);
	}
	
}
