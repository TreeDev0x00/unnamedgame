package com.tree.game.tiles.sets;

import com.tree.game.tiles.Tile;

public class House0TileSet extends TileSet {
	
	public House0TileSet(int startId) {
		super(startId);
		
		int id = getStartId();
		String prefix = "house0";
		
		for (int index = 0; index < 24; index++) {
			addTile(new Tile(id, prefix + index, false, true, 0));
			id++;
		}
		
		for (int index = 24; index < 48; index++) {
			addTile(new Tile(id, prefix + index, true, false, 0));
			id++;
		}
		
		for (int index = 48; index < 51; index++) {
			addTile(new Tile(id, prefix + index, false, false, 0));
			id++;
		}
		
		for (int index = 51; index < 54; index++) {
			addTile(new Tile(id, prefix + index, true, false, 0));
			id++;
		}
		
		for (int index = 54; index < 59; index++) {
			addTile(new Tile(id, prefix + index, false, false, 0));
			id++;
		}
		
		for (int index = 59; index < 62; index++) {
			addTile(new Tile(id, prefix + index, true, false, 0));
			id++;
		}
		
		for (int index = 62; index < 64; index++) {
			addTile(new Tile(id, prefix + index, false, false, 0));
			id++;
		}
		
		setLastId(id);
	}
	
}
