package com.tree.game.tiles.sets;

import java.util.HashSet;
import java.util.Set;

import com.tree.game.tiles.Tile;

public abstract class TileSet {

	private Set<Tile> tiles;
	private int startId;
	private int lastId;
	
	public TileSet(int startId) {
		this.startId = startId;
		this.tiles = new HashSet<>();
		this.setLastId(startId);
	}
	
	public int getStartId() {
		return startId;
	}
	
	public void addTile(Tile tile) {
		tiles.add(tile);
	}
	
	public Set<Tile> getTiles() {
		return tiles;
	}

	public int getLastId() {
		return lastId;
	}

	public void setLastId(int lastId) {
		this.lastId = lastId;
	}
	
}
