package com.tree.game.tiles;

import java.awt.image.BufferedImage;

public class StoredRenderTile {

	private BufferedImage image;
	private int rx;
	private int ry;
	private int rowNumber;
	private int columnNumber;
	private boolean isSolid;
	
	public StoredRenderTile(BufferedImage image, int rx, int ry) {
		this.image = image;
		this.rx = rx;
		this.ry = ry;
	}
	
	public StoredRenderTile(BufferedImage image, int rx, int ry, int rowNumber, int columnNumber, boolean isSolid) {
		this.image = image;
		this.rx = rx;
		this.ry = ry;
		this.rowNumber = rowNumber;
		this.columnNumber = columnNumber;
		this.isSolid = isSolid;
	}
	
	public BufferedImage getImage() {
		return image;
	}
	
	public void setImage(BufferedImage image) {
		this.image = image;
	}
	
	public int getRx() {
		return rx;
	}
	
	public void setRx(int rx) {
		this.rx = rx;
	}
	
	public int getRy() {
		return ry;
	}
	
	public void setRy(int ry) {
		this.ry = ry;
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public int getColumnNumber() {
		return columnNumber;
	}

	public void setColumnNumber(int columnNumber) {
		this.columnNumber = columnNumber;
	}

	public boolean isSolid() {
		return isSolid;
	}

	public void setSolid(boolean isSolid) {
		this.isSolid = isSolid;
	}
	
}
