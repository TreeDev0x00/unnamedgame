package com.tree.game.tiles;

import java.util.HashSet;
import java.util.Set;

import com.tree.game.tiles.sets.House0TileSet;
import com.tree.game.tiles.sets.Tree0TileSet;

public class Tile {

	private static Set<Tile> tiles;
	
	static {
		tiles = new HashSet<>();
		
		tiles.add(new Tile(0, "grassTile", false, false, -1));
		
		Tree0TileSet tree0TileSet = new Tree0TileSet(1);
		tiles.addAll(tree0TileSet.getTiles());
		
		tiles.add(new Tile(tree0TileSet.getLastId(), "", true, true, 0));
		
		House0TileSet house0TileSet = new House0TileSet(tree0TileSet.getLastId() + 1);
		tiles.addAll(house0TileSet.getTiles());
	}
	
	public static final int TILE_WIDTH = 32;
	public static final int TILE_HEIGHT = 32;

	private int id;
	private String assetName;
	private boolean isSolid;
	private boolean renderOverPlayer;
	private int doubleRenderTileId;

	public Tile(int id, String assetName, boolean isSolid, boolean renderOverPlayer, int doubleRenderTileId) {
		this.id = id;
		this.assetName = assetName;
		this.isSolid = isSolid;
		this.renderOverPlayer = renderOverPlayer;
		this.doubleRenderTileId = doubleRenderTileId;
	}

	public String getAssetName() {
		return assetName;
	}

	public boolean shouldRenderOverPlayer() {
		return renderOverPlayer;
	}

	public static Tile getById(int id) {
		for (Tile t : tiles) {
			if (t.getId() == id) {
				return t;
			}
		}
		return null;
	}

	public int getDoubleRenderTileId() {
		return doubleRenderTileId;
	}

	public boolean isSolid() {
		return isSolid;
	}

	public int getId() {
		return id;
	}

}
