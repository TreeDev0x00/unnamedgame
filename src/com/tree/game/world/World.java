package com.tree.game.world;

import java.awt.Graphics;
import java.util.ArrayList;

import com.tree.game.Game;
import com.tree.game.tiles.StoredRenderTile;
import com.tree.game.tiles.Tile;

public class World {

	private Game game;
	private String name;
	private Tile[][] tiles;
	private ArrayList<StoredRenderTile> postRenderTiles = new ArrayList<>();
	
	public World(Game game, String name, int[][] worldData) {
		this.game = game;
		this.name = name;
		loadTileData(worldData);
	}

	public void render(Graphics g) {
		int x = 0;
		int y = 0;

		postRenderTiles.clear();
		
		int rowNum = 0;
		int colNum = 0;
		for (Tile[] row : tiles) {
			for (Tile tile : row) {
				int rx = x + game.getGameCamera().getX();
				int ry = y + game.getGameCamera().getY();
				
				boolean tileOffscreenX = rx + Tile.TILE_WIDTH < 0 || game.getDisplay().getCanvas().getWidth() < rx - Tile.TILE_WIDTH;
			    boolean tileOffscreenY = ry + Tile.TILE_HEIGHT < 0 || game.getDisplay().getCanvas().getHeight() < ry - Tile.TILE_HEIGHT;
			    
				if (!tileOffscreenX && !tileOffscreenY) {
					if (tile.getDoubleRenderTileId() != -1) {
						Tile t = Tile.getById(tile.getDoubleRenderTileId());
						StoredRenderTile doubleRenderTile = new StoredRenderTile(game.getAssets().getTile(t.getAssetName()), rx, ry, rowNum, colNum, t.isSolid());
						g.drawImage(doubleRenderTile.getImage(), rx, ry, null);
					}
					
					if (!tile.shouldRenderOverPlayer()) {
						g.drawImage(game.getAssets().getTile(tile.getAssetName()), rx, ry, null);
					} else {
						postRenderTiles.add(new StoredRenderTile(game.getAssets().getTile(tile.getAssetName()), rx, ry));
					}
				}
				
				x += Tile.TILE_WIDTH;
				colNum++;
			}
			x = 0;
			y += Tile.TILE_HEIGHT;
			colNum = 0;
			rowNum++;
		}
	}
	
	public void postRender(Graphics g) {
		for (StoredRenderTile tile : postRenderTiles) {
			g.drawImage(tile.getImage(), tile.getRx(), tile.getRy(), null);
		}
	}
	
	public boolean checkCollision(float x, float y) {
		float tileX = x / Tile.TILE_WIDTH;
		float tileY = y / Tile.TILE_HEIGHT;
		
		if (tileX < 0 || tileX >= tiles.length) {
			return false;
		}
		
		if (tileY < 0 || tileY >= tiles.length) {
			return false;
		}
		
		return tiles[(int) tileY][(int) tileX].isSolid();
	}
	

	private void loadTileData(int[][] worldData) {
		tiles = new Tile[worldData.length][worldData[0].length];
		for (int row = 0; row < tiles.length; row++) {
			for (int column = 0; column < worldData[row].length; column++) {
				tiles[row][column] = Tile.getById(worldData[row][column]);
			}
		}
	}

	public String getName() {
		return name;
	}

	public Tile[][] getTiles() {
		return tiles;
	}

}
