package com.tree.game.world;

import java.util.ArrayList;
import java.util.List;

import com.tree.game.Game;

public class WorldManager {

	private List<World> worlds;
	private World currentWorld;
	
	public WorldManager(Game game) {
		this.worlds = new ArrayList<>();
		worlds.add(new World(game, "world00", game.getAssets().getLoadedWorldData().get("world00")));
		currentWorld = worlds.get(0);
	}
	
	public World getByName(String name) {
		for (World world : worlds) {
			if (world.getName().equals(name)) {
				return world;
			}
		}
		return null;
	}
	
	public List<World> getWorlds() {
		return worlds;
	}

	public World getCurrentWorld() {
		return currentWorld;
	}

	public void setCurrentWorld(World currentWorld) {
		this.currentWorld = currentWorld;
	}
	
}
