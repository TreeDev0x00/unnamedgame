package com.tree.game.input;

import com.tree.game.Game;
import com.tree.game.input.keyboard.KeyboardInputListener;

public class InputManager {

	private KeyboardInputListener keyboard;
	private InputType type;
	private Game game;

	public InputManager(Game game) {
		this.game = game;
		type = InputType.KEYBOARD;
		keyboard = new KeyboardInputListener();
	}

	public boolean forwardControlPressed() {
		if (game.getInputManager().getInputType() == InputType.KEYBOARD) {
			return checkKeyboardControl("forwardKey");
		}
		return false;
	}
	
	public boolean backwardControlPressed() {
		if (game.getInputManager().getInputType() == InputType.KEYBOARD) {
			return checkKeyboardControl("backwardKey");
		}
		return false;
	}

	public boolean leftControlPressed() {
		if (game.getInputManager().getInputType() == InputType.KEYBOARD) {
			return checkKeyboardControl("leftKey");
		}
		return false;
	}

	public boolean rightControlPressed() {
		if (game.getInputManager().getInputType() == InputType.KEYBOARD) {
			return checkKeyboardControl("rightKey");
		}
		return false;
	}
	
	private boolean checkKeyboardControl(String playerSetting) {
		KeyboardInputListener listener = game.getInputManager().getKeyboardListener();
		return listener.getKeyState(game.getThePlayer().getSettings().getKeyboardInputSetting(playerSetting));
	}

	public InputType getInputType() {
		return type;
	}

	public KeyboardInputListener getKeyboardListener() {
		return keyboard;
	}

}
