package com.tree.game.input;

public enum InputType {
	KEYBOARD, CONTROLLER;
}
