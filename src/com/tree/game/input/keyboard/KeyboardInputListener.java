package com.tree.game.input.keyboard;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;

public class KeyboardInputListener implements KeyListener {

	private Map<Integer, Boolean> keyStates;
	
	public KeyboardInputListener() {
		keyStates = new HashMap<>();
	}
	
	public boolean getKeyState(int key) {
		if (keyStates.get(key) == null) {
			return false;
		}
		return keyStates.get(key);
	}

	@Override
	public void keyPressed(KeyEvent event) {
		keyStates.put(event.getKeyCode(), true);
	}

	@Override
	public void keyReleased(KeyEvent event) {
		keyStates.put(event.getKeyCode(), false);
	}

	@Override
	public void keyTyped(KeyEvent event) {}
	
}
