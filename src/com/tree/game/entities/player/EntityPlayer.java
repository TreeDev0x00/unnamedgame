package com.tree.game.entities.player;

import java.awt.Graphics;

import com.tree.game.Game;
import com.tree.game.entities.Entity;
import com.tree.game.graphics.SpriteSheet;
import com.tree.game.graphics.player.AnimationDirection;
import com.tree.game.graphics.player.BipedalEntityAnimation;
import com.tree.game.settings.Settings;

public class EntityPlayer extends Entity {

	private SpriteSheet sheet;
	private BipedalEntityAnimation animation;
	private Settings settings;
	private int lastWidth;
	private int lastHeight;
	
	public EntityPlayer(Game game, int speed, float x, float y) {
		super(game, speed, x, y);
		game.getGameCamera().setX((int) (x + (game.getDisplay().getCanvas().getWidth() / 2D)));
		game.getGameCamera().setY((int) (y + (game.getDisplay().getCanvas().getHeight() / 2D)));
		this.sheet = new SpriteSheet(game.getAssets().getPlayerSprite("player00"), 64, 64);
		this.animation = new BipedalEntityAnimation(sheet, AnimationDirection.FORWARD, (speed * (60 / game.getGameFps())));
		this.settings = new Settings();
		this.lastWidth = game.getDisplay().getCanvas().getWidth();
		this.lastHeight = game.getDisplay().getCanvas().getHeight();
	}

	public Settings getSettings() {
		return settings;
	}
	
	@Override
	public void update() {
		int canvasWidth = getGame().getDisplay().getCanvas().getWidth();
		int canvasHeight = getGame().getDisplay().getCanvas().getHeight();
		
		if (lastWidth != canvasWidth || lastHeight != canvasHeight) {
			lastWidth = canvasWidth;
			lastHeight = canvasHeight;
			getGame().getGameCamera().setX((int) ((getX() * -1) + (getGame().getDisplay().getCanvas().getWidth() / 2D)));
			getGame().getGameCamera().setY((int) ((getY() * -1) + (getGame().getDisplay().getCanvas().getHeight() / 2D)));
		}
		
		animation.update();
		
		setX((getGame().getDisplay().getCanvas().getWidth() / 2) - getGame().getGameCamera().getX());
		setY((getGame().getDisplay().getCanvas().getHeight() / 2) - getGame().getGameCamera().getY());
		
		getGame().getWorldManager().getCurrentWorld().checkCollision(getX(), getY());
		
		if (getGame().getInputManager().forwardControlPressed()) {
			animation.unfreezeAnimation();
			animation.setAnimationDirection(AnimationDirection.FORWARD);
			double cy = getGame().getGameCamera().getY();
			cy += (getSpeed() * (1/125D)) * (60D / getGame().getGameFps());
			
			float colY = getY() - (sheet.getSpriteHeight() / 2F);
			if (!getGame().getWorldManager().getCurrentWorld().checkCollision(getX(), colY)) {
				getGame().getGameCamera().setY((int) cy);
			}
		} else if (getGame().getInputManager().backwardControlPressed()) {
			animation.unfreezeAnimation();
			animation.setAnimationDirection(AnimationDirection.BACKWARD);
			double cy = getGame().getGameCamera().getY();
			cy -= (getSpeed() * (1/125D)) * (60D / getGame().getGameFps());

			float colY = getY() + (sheet.getSpriteHeight() / 2F);
			if (!getGame().getWorldManager().getCurrentWorld().checkCollision(getX(), colY)) {
				getGame().getGameCamera().setY((int) cy);
			}
		} else if (getGame().getInputManager().leftControlPressed()) {
			animation.unfreezeAnimation();
			animation.setAnimationDirection(AnimationDirection.LEFT);
			double cx = getGame().getGameCamera().getX();
			cx += (getSpeed() * (1/125D)) * (60D / getGame().getGameFps());
			
			float col1X = getX() - (sheet.getSpriteWidth() / 2F);
			float colX = getX() - (sheet.getSpriteWidth() / 2F);
			float colY = getY() + (sheet.getSpriteHeight() / 2F) - 3F;
			if (!getGame().getWorldManager().getCurrentWorld().checkCollision(col1X, getY())) {
				if (!getGame().getWorldManager().getCurrentWorld().checkCollision(colX, colY)) {
					getGame().getGameCamera().setX((int) cx);
				}
			}
		} else if (getGame().getInputManager().rightControlPressed()) {
			animation.unfreezeAnimation();
			animation.setAnimationDirection(AnimationDirection.RIGHT);
			double cx = getGame().getGameCamera().getX();
			cx -= (getSpeed() * (1/125D)) * (60D / getGame().getGameFps());
			
			float col1X = getX() + (sheet.getSpriteWidth() / 2F);
			float colX = getX() + (sheet.getSpriteWidth() / 2F);
			float colY = getY() + (sheet.getSpriteHeight() / 2F) - 3F;
			if (!getGame().getWorldManager().getCurrentWorld().checkCollision(col1X, getY())) {
				if (!getGame().getWorldManager().getCurrentWorld().checkCollision(colX, colY)) {
					getGame().getGameCamera().setX((int) cx);
				}
			}
		} else {
			animation.freezeAnimation();
		}
	}

	@Override
	public void render(Graphics g) {
		int screenX = (getGame().getDisplay().getCanvas().getWidth() / 2) - sheet.getSpriteWidth() / 2;
		int screenY = (getGame().getDisplay().getCanvas().getHeight() / 2) - sheet.getSpriteHeight() / 2;
		g.drawImage(animation.getLatestFrame(), screenX, screenY, null);
	}

}
