package com.tree.game.entities;

import java.awt.Graphics;

import com.tree.game.Game;

public abstract class Entity {

	private Game game;
	private int speed;
	private float x;
	private float y;
	
	public Entity(Game game,int speed, float x, float y) {
		this.game = game;
		this.speed = speed;
		this.x = x;
		this.y = y;
	}
	
	public abstract void update();
	public abstract void render(Graphics g);

	public Game getGame() {
		return game;
	}
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
}
