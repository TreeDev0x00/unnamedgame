package com.tree.game.settings;

import java.util.HashMap;
import java.util.Map;

import com.tree.game.input.InputType;

public class Settings {

	private Map<String, Integer> keyboardInputSettings;
	private InputType inputType;
	
	public Settings() {
		keyboardInputSettings = new HashMap<>();
		loadDefaultKeyboardSettings();
		loadDefaultGeneralSettings();
	}
	
	public int getKeyboardInputSetting(String key) {
		return keyboardInputSettings.get(key);
	}
	
	public InputType getInputType() {
		return inputType;
	}
	
	private void loadDefaultGeneralSettings() {
		inputType = InputType.KEYBOARD;
	}
	
	private void loadDefaultKeyboardSettings() {
		keyboardInputSettings.put("forwardKey", 87);
		keyboardInputSettings.put("backwardKey", 83);
		keyboardInputSettings.put("leftKey", 65);
		keyboardInputSettings.put("rightKey", 68);
	}
	
}
