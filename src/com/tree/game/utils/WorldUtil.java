package com.tree.game.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class WorldUtil {

	private WorldUtil() {
		throw new IllegalStateException("This is a utility class");
	}
	
	public static int[][] loadWorld(InputStream world) {
		List<String> fileData = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(world, StandardCharsets.UTF_8))) {
			String line = br.readLine();
			while (line != null) {
				fileData.add(line);
				line = br.readLine();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		int rows = Integer.parseInt(fileData.get(0).split(" ")[0]);
		int columns = Integer.parseInt(fileData.get(0).split(" ")[1]);
		int[][] result = new int[rows][columns];
		
		int colIndex = 0;
		for (int index = 1; index < fileData.size(); index++) {
			String data = fileData.get(index);
			int currentIndex = 0;
			for (String numberStr : data.split(" ")) {
				result[colIndex][currentIndex] = Integer.parseInt(numberStr);
				currentIndex++;
			}
			colIndex++;
		}
		
		return result;
	}
	
}
