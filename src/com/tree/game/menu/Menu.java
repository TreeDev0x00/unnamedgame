package com.tree.game.menu;

import java.awt.Graphics;

import com.tree.game.Game;

public abstract class Menu {

	private Game game;
	
	public Menu(Game game) {
		this.game = game;
	}
	
	public Game getGame() {
		return game;
	}
	
	public abstract void update();
	public abstract void render(Graphics g);
	
}
