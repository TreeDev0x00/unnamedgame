package com.tree.game.menu;

import java.awt.Color;
import java.awt.Graphics;

import com.tree.game.Game;

public class GameMenu extends Menu {

	public GameMenu(Game game) {
		super(game);
	}
	
	@Override
	public void update() {
		getGame().getThePlayer().update();
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.black);
		g.fillRect(0, 0, getGame().getDisplay().getCanvas().getWidth(), getGame().getDisplay().getCanvas().getHeight());
		getGame().getWorldManager().getCurrentWorld().render(g);
		getGame().getThePlayer().render(g);
		getGame().getWorldManager().getCurrentWorld().postRender(g);
	}

}
