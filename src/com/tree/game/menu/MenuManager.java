package com.tree.game.menu;

import com.tree.game.Game;

public class MenuManager {

	private GameMenu gameMenu;

	private Menu currentMenu;
	
	public MenuManager(Game game) {
		gameMenu = new GameMenu(game);
	}
	
	public GameMenu getGameMenu() {
		return gameMenu;
	}

	public Menu getCurrentMenu() {
		return currentMenu;
	}

	public void setCurrentMenu(Menu currentMenu) {
		this.currentMenu = currentMenu;
	}
	
}
